
[![](https://gitlab.com/ddokk/gfx/-/raw/main/g/b1.png)](https://gitlab.com/ddokk/tv-fqig)

----

<a href="https://youtu.be/feG4oKoC8Fc" target="_blank">
<img src="./filez/yt.png"></img>
</a>

---
<h1 align="center"> 🔖: TV-FQIG </h1>
<h2 align="center"> Notes on Famous Quotes of Imam Ghazzali  </h2>

----

_This repo is best viewed in DarkMode_ 

----   
1. [What ?](#what-)
   1. [Vid](#vid)
      1. [URL](#url)
2. [Notes](#notes)
   1. [Regarding Provision Interruption](#regarding-provision-interruption)
   2. [12 Enemies which you cannot see](#12-enemies-which-you-cannot-see)
   3. [Types of people](#types-of-people)
   4. [Relationship between pain and attachments](#relationship-between-pain-and-attachments)
   5. [Friends in the grave](#friends-in-the-grave)
   6. [Regarding Sorrows](#regarding-sorrows)
   7. [Good Character Traits](#good-character-traits)
   8. [Knowledge of `ALLAH`](#knowledge-of-allah)
   9. [Mental Discipline in Salat](#mental-discipline-in-salat)
3. [Online Complete Works](#online-complete-works)
4. [Repository Arbitrary Authentication](#repository-arbitrary-authentication)
5. [ToDo List](#todo-list)
6. [Issues and PR Invitation](#issues-and-pr-invitation)
7. [Cite](#cite)
8. [Verification](#verification)

----

# What ? 

This repo is various illustrations and notes of interest from the youtube video as mentioned in the next section. It will contain the following:
1. Diagrammatic representation of ideas. 
2. Open-Source references which are either purely for reference or were used for building the diagrams.
3. These are my personal notes and can be either partially or completely inaccurate.
4. _Imam Al Ghazali_ will be referenced as `IAG` through out this repo
5. Text marked with the emoji - 🤖 - Indicates my personal opinion. 
6. Now every point in the talk has been documented, only those concepts that multiple branches
7. Most probably this repo will be instate of perpetual WIP, in order to find exactly where these famous quotes have been found and in what context have they been mentioned. In the mean time this information is still relevant as a form of a gist or concise summary of the key ideas mentioned.
8. Note - All the diagrams in this repo are large, and they have bene resized for being viewable on a standard screen. You can download then and zoom in to see the details more clearly, or right click and open image ina  new tab.


_Diagrams are in a spoiler tag click "▸" to reveal_ 

## Vid

<details>

<summary> 📹 Watch Vid </summary>

![](./filez/fqig.mp4)

</details>

### URL 
 

<h1>https://youtu.be/feG4oKoC8Fc</h1>


# Notes 

>Personal notes in the form of diagrams, Code can be found [HERE](./filez/), usage is governed by [`LICENSE`](./LICENSE).

## Regarding Provision Interruption 

> 🤖 IAG interpretation is quite similar to various hadith and other derived islamic literature on this subject of Qadr in relation to a worshipper

<details>

<summary> 🖼️ Provision Interruption </summary>

<img src="./filez/pi.png" width=400>

</details>

## 12 Enemies which you cannot see 

> 🤖 - Many texts that have the type of methodology IAG uses, also highlight this concept of the HEART and its purification. Where the identified characteristic are blameworthy. One of the goals of a worshipper is to rid themselves of these diseases, which will inturn purify it. This pure heart is a necessary precondition to win "ﷲ" approval, which would then make receiving the reward justified in the hereafter. This process is also referred to as Tazkiya (purification of the soul/nafs - "تزكية"). This [`Youtube - Playlist`](https://youtube.com/playlist?list=PLYZxc42QNctWeXvciIWtItbjhod9PjcCN) was being watched by the author during the writing of this repo, which was the 11th of Ramadan 1444 (Mon, 03 Apr 2023 17:39:47 +0000 (now)).

<details>

<summary> 🖼️ 12 Enemies </summary>

<img src="./filez/12e.png" width=600>

</details>

## Types of people

> 🤖 - Here various types of personalities have been described, lots of this information perfectly explains certain personality traits that are even considered to be reprehensible in 2023. Notably number A2.4 is the trait which has been exploited by social media , and as mentioned in by IAG these are the maximum types of traits that exist today. Who are just a all show and tell devoid of any substance, ethic or moral concern. 
> 🤖 - Object of Appetite means that thing which satisfies a desire which arises, for eg: When you hungry you would want your favorite food to satisfy your hunger

<details>

<summary> 🖼️ : Types of people</summary>

<img src="./filez/typ.png">

</details>

## Relationship between pain and attachments 

> 🤖 attempting to make a math formula for this. Fundamentally IAG mentions that during the death throes, the severity of the pain of separation for something will be in proportion to the attachment of the heart to it. 

----

$$
\left\rangle p \propto ha \right\rangle \Longrightarrow  \oslash \\
\text{p = Intensity of pangs of death} \\
\text{ha = Intensity of attachment to object } \\
\left\rangle \right\rangle = \text{death throes} \\
\oslash = \mathfrak{death}
$$

----

## Friends in the grave

<details>

<summary> 🖼️ : Friends in Grave </summary>

![](./filez/fg.png)

- [`Image URL`](https://excalidraw.com/#json=KX8lHgNfKbT1Sr3CKNarD,K-23BuYny3oN79ib1_e1ZQ) - Exit link
- [`fgpng.excalidraw`](./filez/fgpng.excalidraw) - source `json` for import and edit

</details>

## Regarding Sorrows

----

$$ 
\large \color{C6DC67}\colorbox{black}{\text{If you want to be free from all affliction and suffering}} \\
\large \color{98CC70}\colorbox{black}{\textbf{HOLD FAST TO ALLAH AND TURN WHOLLY TO HIM AND NO ONE ELSE}} \\
\large \color{ED017D}\colorbox{black}{\text{Indeed ALL suffering comes from this, that you dont turn towards to ALLAH}}
$$

---

## Good Character Traits

> 🤖 - Character traits mentioned are very thorough from a personality analysis perspective. Some of them have been repeated in multiple different books on a similar subject

<details>

<summary>
Good Character Traits Mentioned 
</summary>

![](./filez/gc.png)


</details>

## Knowledge of `ALLAH`

> 🤖 - Arises from the study and contemplation of our own bodies, note it is this appreciation that leads to understand why human are an invaluable debt to its creator and gratitude should be for them also, in addition to the provisions, this understanding is often overlooked. Because our mortal enemy makes us forget to pay attention to these details. 

<details>

<summary> Illustration of concepts </summary>

![](./filez/kog.png)

</details>

## Mental Discipline in Salat

> 🤖 - Mental discipline involved in doing the Salat, I have also come across this type of description in other sources, which most of them have similar elements

<details>

<summary>
Salat Discipline 
</summary>

![](./filez/IGP.png)

N | Url
|:--:|:--:|
`Excalidraw Import` | [`Excalidraw Share URL Edittable`](https://excalidraw.com/#json=ycg7cFwbhuoL2Qj3WEOb7,0n0bHNyqIwrzgNVzWSdH7Q)
`Excalidraw JSON` | [`JSON`](./filez/IGP-2023-04-10-0224.excalidraw)

</details>


---- 
----

# Online Complete Works

- These will be the complete works which are open source
- Some of these sources have been used here in this repo
  
$$\color{red}{Sources \ have \ not \ been \ verified \ by \ me \ DYOR}$$

N | URL | Description 
|:--:|:--:|:--:|
*Ihya Ulum al din* <br> _Revivial of the religious sciences_ | [`ghazali.org`](https://www.ghazali.org/rrs-ovr/) | English translated works which were done <br> as part of academic work 
*Ihya Ulum Al Din Vol 1 to 4* | [`Archive.org`](https://archive.org/details/IhyaUlumAlDinVol1/Ihya%20Ulum%20Al%20Din%20Vol%201/) | Vol 1 to 4 PDF <br> Scanner content 

# Repository Arbitrary Authentication 

> 🤖 - Authenticity of this repository by its author are recorded as a encoded message on the sepolia network as burn transaction. This is just a method to determine that the content is indeed by the author of this repo, in those cases where the information in this repo is cloned and used elsewhere. 

<details>

<summary>
__Verfication Details__
</summary>

## Onchain Verification Messages 

```ml 
https://gitlab.com/ddokk/tv-fqig
Al-Ghazali Famous Quotes Study

********************************************************************************

 ╔╦╗ ╔═╗ ╔═╗ ╦ ╦═╗ ╔═╗ ╔═╗      ╔╦╗ ╔═╗ ╦╔═ ╔═╗ ╔═╗     
  ║║ ║╣  ╚═╗ ║ ╠╦╝ ║╣  ╚═╗      ║║║ ╠═╣ ╠╩╗ ║╣  ╚═╗     
 ═╩╝ ╚═╝ ╚═╝ ╩ ╩╚═ ╚═╝ ╚═╝      ╩ ╩ ╩ ╩ ╩ ╩ ╚═╝ ╚═╝     
╔═╗ ╦   ╔═╗ ╦  ╦ ╔═╗      ╔═╗ ╦ ╦ ╔╦╗      ╔═╗ ╔═╗      ╦╔═ ╦ ╔╗╔ ╔═╗ ╔═╗     
╚═╗ ║   ╠═╣ ╚╗╔╝ ║╣       ║ ║ ║ ║  ║       ║ ║ ╠╣       ╠╩╗ ║ ║║║ ║ ╦ ╚═╗     
╚═╝ ╩═╝ ╩ ╩  ╚╝  ╚═╝      ╚═╝ ╚═╝  ╩       ╚═╝ ╚        ╩ ╩ ╩ ╝╚╝ ╚═╝ ╚═╝     
╦╔═ ╦ ╔╗╔ ╔═╗ ╔═╗      ╔═╗ ╦ ╦ ╔╦╗      ╔═╗ ╔═╗      ╔═╗ ╦   ╔═╗ ╦  ╦ ╔═╗     
╠╩╗ ║ ║║║ ║ ╦ ╚═╗      ║ ║ ║ ║  ║       ║ ║ ╠╣       ╚═╗ ║   ╠═╣ ╚╗╔╝ ║╣      
╩ ╩ ╩ ╝╚╝ ╚═╝ ╚═╝      ╚═╝ ╚═╝  ╩       ╚═╝ ╚        ╚═╝ ╩═╝ ╩ ╩  ╚╝  ╚═╝     

********************************************************************************
``` 

Chain | Hash
|:--:|:--:|
[`Sepolia`](https://sepolia.etherscan.io/tx/0x7334501a8567096392dc94d1f15c699d9f68798b76127437806a79317e5e3750) | [`0x7334501a8567096392dc94d1f15c699d9f68798b76127437806a79317e5e3750`](https://sepolia.etherscan.io/tx/0x7334501a8567096392dc94d1f15c699d9f68798b76127437806a79317e5e3750)
[`Matic`](https://mumbai.polygonscan.com/tx/0x4a35490d645d051a42d0abd275ed1ab79daeea772b1b347eb03e128327b0740d) | [`0x4a35490d645d051a42d0abd275ed1ab79daeea772b1b347eb03e128327b0740d`](https://mumbai.polygonscan.com/tx/0x4a35490d645d051a42d0abd275ed1ab79daeea772b1b347eb03e128327b0740d)
[`BinanceTestNet`](https://testnet.bscscan.com/tx/0xfd1c63bc5a4d7ae8287bc7498ef3f44ac9a3249b953cfcd28c2f02be45fb4394) | [`0xfd1c63bc5a4d7ae8287bc7498ef3f44ac9a3249b953cfcd28c2f02be45fb4394`](https://testnet.bscscan.com/tx/0xfd1c63bc5a4d7ae8287bc7498ef3f44ac9a3249b953cfcd28c2f02be45fb4394)


## RepoSnap 

img | hash
|:--:|:--:|
![](./filez/fqigCodeRepo.png) | [`sha512`](./filez/fqigCodeRepoSha512.txt)

</details>

# ToDo List 

- [ ] Find Exact page number where the actual quotes are mentioned
- [ ] Add own analysis for general audience consumption
- [ ] Quran and hadith sources for grave and explanation

# Issues and PR Invitation 

- I invite PRs and Issue which will be attended to hopefully with some cadence
- Since this is not primar 

# Cite 

$$ 
\textcolor{white}{\colorbox{green}{\textbf{TV, https://gitlab.com/ddokk/tv-fqig}}}
$$
