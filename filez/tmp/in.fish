#!/bin/fish 

# Cargo Installs
curl https://gitlab.com/liqowomo/lq-on-bk-1/-/raw/main/l/fixrs.fish | fish

# pnpm updates
curl https://gitlab.com/liqowomo/lq-on-bk-1/-/raw/main/l/pnp.fish | fish 

# Installing Important stuff 
curl https://gitlab.com/liqowomo/lq-on-bk-1/-/raw/main/l/pnp.fish | fish 
curl https://gitlab.com/liqowomo/lq-on-bk-1/-/raw/main/l/st.sh | bash 
curl https://gitlab.com/liqowomo/lq-on-bk-1/-/raw/main/l/fo.sh | bash 


echo -e "Add this to ~/.config/starship.toml"
echo -e "[⋆﹥━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━﹤⋆](fg:125)"